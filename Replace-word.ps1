<#

.SYNOPSIS
This function will let you replace words within files.

.DESCRIPTION
with this function you search for a word and replace that with another one within a file.
this is case sensitive.

.EXAMPLE
Replace-Word -Path . -Searchword 'COOKBOOK' -Replaceword 'RECIPE'

.KNOWN Replaceables
#COOKBOOK
#NAME <EMAIL>
#EMAIL
#YEAR
#COMPANY
#DESCRIPTION
#source_url

.NOTES

Created by:
Ferry Blankendaal

.VERSION 
0.1 - First version of the script.

#>

Function Replace-Word {
[CmdletBinding()]
    Param(
     
       [Parameter(Mandatory=$true)]
       [string]$Path,
    
       [Parameter(Mandatory=$true)]
       [string]$Searchword,
    
       [Parameter(Mandatory=$true)]
       [string]$Replaceword
    )
    
    $ErrorActionPreference = "SilentlyContinue"
    
    (Get-ChildItem -Path $path -Recurse) | ForEach { (Get-Content -Path $_.FullName).Replace($Searchword,$Replaceword)  | Set-Content -Path $_.FullName }
}