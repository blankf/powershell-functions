function change-chefenv {
    Param
    (
        [Parameter(Mandatory=$true)]
        [ValidateSet("-", "-")]
        $env,
        [ValidateScript({Test-Path $_})]
        [string]
        $dir = "$env:userprofile\.chef\knife.rb"
    )

    $lines = (Get-Content $dir)
    if (test-path "$dir.bak") {Remove-Item "$dir.bak" -Force}
    Move-Item -Path $dir -Destination "$dir.bak"
    
        foreach ($line in $lines) {
            if ($line -like 'chef_server_url*') {
                $line = $line -replace '\/(\w+)"',"/$env`""
            }
            $line | out-file $dir -Append -Encoding ascii
        }
}