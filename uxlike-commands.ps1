#Unix Like commands
function which($name) {
  Get-Command $name | Select-Object -ExpandProperty Definition
}

function touch($file) {
  "" | Out-File $file -Encoding ASCII
}

function pnamekill($name) {
  get-process -name $name -ErrorAction SilentlyContinue | kill
}

function pidkill($id) {
  get-process -id $id -ErrorAction SilentlyContinue | kill
}

function sudo {
  $file, [string]$arguments = $args;
  $psi = new-object System.Diagnostics.ProcessStartInfo $file;
  $psi.Arguments = $arguments;
  $psi.Verb = "runas";
  $psi.WorkingDirectory = get-location;
  [System.Diagnostics.Process]::Start($psi) >> $null
}

function grep {
    [CmdletBinding()]
    param
    (
        [Parameter(Mandatory=$True,ValueFromPipeline=$True)]
        $pipe,
        [Parameter(Mandatory=$True,Position=0)]
        $string
    )
    process
    {
        $pipe | Where-Object { $_ | Select-String $string}
    }
}