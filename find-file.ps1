function find-file($name) {
  ls -recurse -filter "*${name}*" -ErrorAction SilentlyContinue | foreach-object {
    $place_path = $_.directory
    echo "${place_path}\${_}"
  }
}
